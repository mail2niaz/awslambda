﻿"use strict";

var config = {
    "templateBucket" : "bcwcot.project.src.zip",
    "templateKey" : "Templates/Template.html",
	"targetAddressDev" : "v4.niaz@excelblaze.com",
    "targetAddress" : "jthompson@cypressesolutions.com",
    "fromAddress": "<jthompson@cypressesolutions.com>",
    "defaultSubject" : "Email From {{name}}"
}

module.exports = config
