// dependencies
var async = require('async');
var path = require('path');
var AWS = require('aws-sdk');
var gm = require('gm').subClass({
    imageMagick: true
});
var PDFImage = require("pdf-image").PDFImage;
var util = require('util');
var fs = require('fs');
var mktemp = require("mktemp");


// get reference to S3 client
var s3 = new AWS.S3();
exports.handler = function(event, context) {
    // Read options from the event.
    console.log("Reading options from event:\n", util.inspect(event, {
        depth: 5
    }));
    var srcBucket = event.Records[0].s3.bucket.name;
    // Object key may have spaces or unicode non-ASCII characters.
    var srcKey = decodeURIComponent(event.Records[0].s3.object.key.replace(
        /\+/g, " "));

    //	var dstBucket = 'bcw.cot.test.niaz';
    var dstBucket = 'bcwcot.output';


    // Sanity check: validate that source and destination are different buckets.

    if (srcBucket == dstBucket) {
        console.error("Destination bucket must not match source bucket.");
        return;
    }


    var image_modify = {
        width: 300,
        rotate: 270,
        dstnKey: srcKey,
        destinationPath: "thumbnail"
    };
    //var _sizesArray = [_800px, _500px, _200px, _45px];
    var _sizesArray = [image_modify];
    var len = _sizesArray.length;
    var format = event.format;
    console.log(len);
    console.log(srcBucket);
    console.log(srcKey);

    var key = 0;
    var fileName = path.basename(srcKey);
    console.log("fileName=" + fileName);
    var filepathInit = replaceAll(fileName, '_', '\/');
    //var filepathInit = filepathInit1.replace('\.', '\/');
    console.log("filepathInit=" + filepathInit);
	console.log("..Test removing underscore..")
    //var lastIndexOfSlash = filepathInit.lastIndexOf("/");
    //var filepath = filepathInit.substring(0, (lastIndexOfSlash)) + "/";
	var secondIndexOfSlash = getPosition(filepathInit,'\/',2);
	var filepath = filepathInit.substring(0, secondIndexOfSlash) + "/";
    console.log("filepath=" + filepath);

//Upload Original
    async.waterfall([

        function download(next) {
            s3.getObject({
                Bucket: srcBucket,
                Key: srcKey
            }, next);
        },
        function uploadOriginal(response, next) {
            s3.putObject({
                Bucket: dstBucket,
                Key: filepath + fileName.slice(0, -4) +
                    ".pdf",
                Body: response.Body,
                ContentType: 'PDF',
                ACL: 'public-read'
            }, next);
        }
    ]);

//Transform and upload
    async.waterfall([

        function download(next) {
            s3.getObject({
                Bucket: srcBucket,
                Key: srcKey
            }, next);
        },
        function processTempFile(response, next) {
            var temp_file, image;
            temp_file = mktemp.createFileSync("/tmp/XXXXXXXXXX.pdf");
            fs.writeFileSync(temp_file, response.Body);
            image = gm(temp_file + "[0]").flatten().colorspace("CMYK");
            image.toBuffer("JPG", function(err, buffer) {
                if (err) {
                    next(err);
                } else {
                    console.log('inside uploadImage gm...');
                    next(null, response.ContentType, buffer);
                }
            });
        },

        function convert(contentType, data, next) {
            // convert eps images to png

            console.log("Conversion");
            gm(data).antialias(true).density(
                300).toBuffer('JPG', function(err,
                buffer) {
                if (err) {
                    //next(err);
                    next(err);
                } else {
                    next(null, buffer);
                    //next(null, 'done');
                }
            });
        },
        function process(response, next) {
            console.log("process image");
            console.time("processImage");
            // Transform the image buffer in memory.
            //gm(response.Body).size(function(err, size) {
            gm(response).size(function(err, size) {
                //console.log("buf content type " + buf.ContentType);
                // Infer the scaling factor to avoid stretching the image unnaturally.

                var width = size.width;
                var height = size.height;
                console.log("run " + key +
                    " width : " + width);
                console.log("run " + key +
                    " height : " + height);
                var index = key;
                //this.resize({width: width, height: height, format: 'jpg',})
                if (height > width) {
                    console.log("i am rotating...");
                    this.rotate('white', _sizesArray[key].rotate);

                    console.log("after********************");
                    console.log("run " + key +
                        " width : " + width);
                    console.log("run " + key +
                        " height : " + height);
                    this.resize(_sizesArray[key].width).toBuffer(
                        'JPG',
                        function(err,
                            buffer) {
                            if (err) {
                                //next(err);
                                console.log('calling next err...' + err);
                                next(err);
                            } else {


                                console.log('calling next...');
                                next(null,
                                    buffer,
                                    key);
                            }
                        });
                } else {
                    console.log("no rotating...");
                    console.log("after********************");
                    console.log("run " + key +
                        " width : " + width);
                    console.log("run " + key +
                        " height : " + height);

                    this.resize(_sizesArray[key].width).toBuffer(
                        'JPG',
                        function(err,
                            buffer) {
                            if (err) {
                                //next(err);
                                console.log('calling next err...' + err);
                                next(err);
                            } else {


                                console.log('calling next...');
                                next(null,
                                    buffer,
                                    key);
                            }
                        });
                }
                console.log('calling nextss...');

            });
        },
        function upload(data, index, next) {
            console.log("upload....");

            // Stream the transformed image to a different folder.
            try {
                s3.putObject({
                    Bucket: dstBucket,
                    ACL: 'public-read',
                    Key: filepath + "thumbnail" +
                        ".png",
                    Body: data,
                    ContentType: 'PNG'
                }, next);

            } catch (er) {
                console.log('uploadImage...' + er);
            }
        }
    ]);

    };

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
function getPosition(str, m, i) {
   return str.split(m, i).join(m).length;
}