// dependencies
var async = require('async');
var path = require('path');
var AWS = require('aws-sdk');
var gm = require('gm').subClass({
    imageMagick: true
});
var PDFImage = require("pdf-image").PDFImage;
var util = require('util');
var fs = require('fs');
var mktemp = require("mktemp");


// get reference to S3 client
var s3 = new AWS.S3();


var params = {
    Bucket: 'bcwcot.output',
    Prefix: '',
};
 var s3DataContents = [];
exports.handler = (event, context, callback) => {
   s3DataContents = [];
  s3ListObjects(params, s3Print,callback);
};

function s3ListObjects(params, print,callback) {
    s3.listObjects(params, function(err, data) {
        if (err) {
            console.log("listS3Objects Error:", err);
        } else {
            var contents = data.Contents;
            s3DataContents = s3DataContents.concat(contents);
            if (data.IsTruncated) {
                // Set Marker to last returned key
                params.Marker = contents[contents.length-1].Key;
                s3ListObjects(params, print(callback));
            } else {
                print(callback);
            }
        }
    });
}
function s3Print(callback) {
 
/*   
   if (program.al) {
        // --al: Print all objects
        console.log(JSON.stringify(s3DataContents, null, "    "));
    } else {
        // --b: Print key only, otherwise also print index 
        var i;
        for (i = 0; i < s3DataContents.length; i++) {
            var head = !program.b ? (i+1) + ': ' : '';
            console.log(head + s3DataContents[i].Key);
        }
    }
	*/
	console.log("*******************************************");
	
	//var output = JSON.stringify(s3DataContents);
	var output = s3DataContents;
	console.log(output);
	/*
	var err = [];
	err.status  = 'Something went wrong';
	*/
	callback(null, output);
	
}


function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
function getPosition(str, m, i) {
   return str.split(m, i).join(m).length;
}